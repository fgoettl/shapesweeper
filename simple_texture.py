# -*- coding: utf-8 -*-
"""
@author: Florian
"""

import pyglet
import pyglet.gl as GL
import ctypes as C
from math import sqrt, sin, cos, pi
from random import randint

from minimal_utility import gen_vao, gen_vbo, fill_vbo, gen_ebo, fill_vbo_c
from shaderloader import ShaderProgram


def hexcoords(x, y, k, r=0.1, d=0.02):
    center_dist = r * sqrt(3) + d
    center_x, center_y = ((0.5 * y + x) * center_dist, sqrt(3) / 2 * y * center_dist)
    intern_x, intern_y = (cos(pi / 3 * k + pi / 6), sin(pi / 3 * k + pi / 6))
    return center_x + intern_x * r, center_y + intern_y * r, 0, 1 - (intern_y + 1) / 2, (intern_x + 1) / 2


def hexcenter(x, y, r=0.1, d=0.02):
    center_dist = r * sqrt(3) + d
    return (0.5 * y + x) * center_dist, sqrt(3) / 2 * y * center_dist


def hexindices(n):
    ind = [0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5]
    return [i + 6 * n for i in ind]


class Hextile:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.neighbours = []
        self.bomb = False
        self.adj_bombs = 0
        self.status = 0
        self.ID = -1
        self.Renderer = None

    def find_neighbours(self, tiles):
        self.neighbours = []
        for t in tiles:
            if ((t.x == self.x + 1 or t.x == self.x - 1) and t.y == self.y) or (
                    (t.y == self.y - 1 or t.y == self.y + 1) and t.x == self.x) or (
                    t.x == self.x - 1 and t.y == self.y + 1) or (t.x == self.x + 1 and t.y == self.y - 1):
                self.neighbours.append(t)

    def set_bomb(self, b=True):
        self.bomb = b

    def calc_adj_bombs(self):
        self.adj_bombs = sum([t.bomb for t in self.neighbours])

    def get_data(self, r=0.05, d=0.01):
        vertices = []
        for k in range(6):
            vertices.extend(hexcoords(self.x, self.y, k, r, d))
        indices = hexindices(self.ID)
        status = [self.status for _ in range(6)]
        if self.bomb:
            underlying = [7] * 6
        else:
            underlying = [self.adj_bombs for _ in range(6)]
        return vertices, indices, status, underlying

    def get_status(self):
        return [self.status for _ in range(6)]

    def is_hit(self, x, y, r=0.05, d=0.01):
        sx, sy = hexcenter(self.x, self.y, r, d)
        dx = abs(x - sx)
        dy = abs(y - sy)
        b1 = (dx < sqrt(3) / 2 * r)
        b2 = (dy < -r / sqrt(3) * dx + r)
        return b1 and b2

    def reveal(self):
        if self.bomb and self.status == 0:
            print("LOST")
            self.Renderer.set_exploded()
        if self.status == 0:
            self.status = 1
            self.Renderer.changed = True
            if self.adj_bombs == 0 and not self.bomb:
                for n in self.neighbours:
                    n.reveal()

    def flag(self):
        if self.status == 0:
            self.status = 2
            self.Renderer.changed = True
        elif self.status == 2:
            self.status = 0
            self.Renderer.changed = True


class HextileRenderer:
    def __init__(self, tiles):
        self.tiles = tiles
        for i, t in enumerate(self.tiles):
            t.ID = i
            t.Renderer = self
        self.changed = False
        self.exploded = False
        xvertices = []
        xindices = []
        xstatus = []
        xunderlying = []
        self.element_num = len(self.tiles) * 12
        for t in self.tiles:
            v, i, s, u = t.get_data()
            xvertices.extend(v)
            xindices.extend(i)
            xstatus.extend(s)
            xunderlying.extend(u)
        self.vertices = (C.c_float * len(xvertices))(*xvertices)
        self.indices = (C.c_uint * len(xindices))(*xindices)
        self.status = (C.c_int * len(xstatus))(*xstatus)
        self.underlying = (C.c_int * len(xunderlying))(*xunderlying)

        self.vao = gen_vao()
        GL.glBindVertexArray(self.vao)
        vbo = gen_vbo()
        fill_vbo_c(vbo, self.vertices)
        GL.glVertexAttribPointer(0, 3, GL.GL_FLOAT, GL.GL_FALSE, 5 * C.sizeof(C.c_float()), C.c_void_p(0))
        GL.glVertexAttribPointer(1, 2, GL.GL_FLOAT, GL.GL_FALSE, 5 * C.sizeof(C.c_float()),
                                 C.c_void_p(3 * C.sizeof(C.c_float())))
        GL.glEnableVertexAttribArray(0)
        GL.glEnableVertexAttribArray(1)

        vbo = gen_vbo()
        fill_vbo_c(vbo, self.status)
        self.status_vbo = vbo
        GL.glVertexAttribIPointer(2, 1, GL.GL_INT, 1 * C.sizeof(C.c_int()), C.c_void_p(0))
        GL.glEnableVertexAttribArray(2)

        vbo = gen_vbo()
        fill_vbo_c(vbo, self.underlying)
        GL.glVertexAttribIPointer(3, 1, GL.GL_INT, 1 * C.sizeof(C.c_int()), C.c_void_p(0))
        GL.glEnableVertexAttribArray(3)

        ebo = gen_ebo()
        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, ebo)
        GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, C.sizeof(self.indices), C.byref(self.indices),
                        GL.GL_STATIC_DRAW)

        # Compile the ShaderProgram and put in the texture
        self.shaderProgram = ShaderProgram("shaders/tile.vs", "shaders/tile.fs")

    def set_exploded(self, b=True):
        self.exploded = b
        self.shaderProgram.use()
        self.shaderProgram.setBool("exploded", b)

    def draw(self):
        if self.changed:
            xstatus = []
            for t in self.tiles:
                xstatus.extend(t.get_status())
            self.status = (C.c_int * len(xstatus))(*xstatus)
            fill_vbo_c(self.status_vbo, self.status)
            self.changed = False
        self.shaderProgram.use()
        GL.glBindVertexArray(self.vao)
        GL.glDrawElements(GL.GL_TRIANGLES, self.element_num, GL.GL_UNSIGNED_INT, C.c_void_p(0))


def shuffle(L):
    for i in range(len(L)):
        j = randint(i, len(L) - 1)
        t = L[i]
        L[i] = L[j]
        L[j] = t


class Own_Window(pyglet.window.Window):
    def __init__(self, **kwargs):
        pyglet.window.Window.__init__(self, **kwargs)

        # Hextiles erstellen
        hextiles = []
        for x in range(-30, 30):
            for y in range(-30, 30):
                cx, cy = hexcenter(x, y)
                if cx * cx + cy * cy <= 3.2:
                    hextiles.append(Hextile(x, y))
        # Hextiles mit Bomben fuellen
        bomb_num = 70
        shuffle(hextiles)
        for i, t in zip(range(bomb_num), hextiles):
            t.set_bomb(True)
        for t in hextiles:
            t.find_neighbours(hextiles)
            t.calc_adj_bombs()
        self.hexrenderer = HextileRenderer(hextiles)
        self.hextiles = hextiles

        GL.glClearColor(0.0, 0.0, 0.0, 1)

    def on_draw(self):
        GL.glClear(GL.GL_COLOR_BUFFER_BIT)
        self.hexrenderer.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        if button == pyglet.window.mouse.LEFT:
            nx = (x - 450) / 450
            ny = (y - 450) / 450
            for t in self.hextiles:
                if t.is_hit(nx, ny):
                    t.reveal()
                    break
        elif button == pyglet.window.mouse.RIGHT:
            nx = (x - 450) / 450
            ny = (y - 450) / 450
            for t in self.hextiles:
                if t.is_hit(nx, ny):
                    t.flag()
                    break


def main():
    config = GL.Config(major_version=3, double_buffer=True, depth_size=24)
    Own_Window(width=900, height=900, config=config, caption="Hexsweeper!")
    pyglet.app.run()


if __name__ == '__main__':
    main()