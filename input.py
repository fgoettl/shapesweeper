from geometry import Point2d


class Clickable:

    def bounding_box(self) -> (float, float, float, float):
        raise NotImplementedError

    def is_hit_by(self, p: Point2d) -> bool:
        raise NotImplementedError
