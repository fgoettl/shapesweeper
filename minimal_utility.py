import pyglet.gl as GL
import ctypes as C
from PIL import Image


def gen_vbo():
    vbo = C.c_uint()
    GL.glGenBuffers(1, C.byref(vbo))
    return vbo


def fill_vbo(vbo, vertex_data, type=C.c_float):
    c_vertex_data = (type * len(vertex_data))(*vertex_data)
    GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo)
    GL.glBufferData(GL.GL_ARRAY_BUFFER, len(vertex_data) * C.sizeof(type()), C.byref(c_vertex_data),
                    GL.GL_STATIC_DRAW)


def fill_vbo_c(vbo, c_vertex_data):
    GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo)
    GL.glBufferData(GL.GL_ARRAY_BUFFER, C.sizeof(c_vertex_data), C.byref(c_vertex_data),
                    GL.GL_STATIC_DRAW)


def gen_vao():
    vao = C.c_uint()
    GL.glGenVertexArrays(1, C.byref(vao))
    return vao


def gen_ebo():
    ebo = GL.GLuint()
    GL.glGenBuffers(1, C.byref(ebo))
    return ebo


def loadpic(file):
    im = Image.open(file)
    pix = im.load()
    xpixels = []
    for i in range(im.size[0]):
        for j in range(im.size[1]):
            xpixels.append(pix[i, j][0] / 255.0)
            xpixels.append(pix[i, j][1] / 255.0)
            xpixels.append(pix[i, j][2] / 255.0)
    pixels = (C.c_float * len(xpixels))(*xpixels)
    return pixels, im.size[0], im.size[1]


def gen_texture(path):
    pixels, width, height = loadpic(path)
    texture = C.c_uint()
    GL.glGenTextures(1, C.byref(texture))
    GL.glBindTexture(GL.GL_TEXTURE_2D, texture)
    GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, width, height, 0, GL.GL_RGB, GL.GL_FLOAT, pixels)
    GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
    return texture
