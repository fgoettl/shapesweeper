from __future__ import annotations
from typing import Callable
import math

from geometry import Point2d
from planar_subdivision import PlanarSubdivision


def point_edge_tiling(points: list[Point2d], edges: list[(Point2d, Point2d)]) -> PlanarSubdivision:
    psd: PlanarSubdivision = PlanarSubdivision()
    points_and_edges: list[(Point2d | (Point2d, Point2d), bool)] = [(p, False) for p in points] + [(e, True) for e in
                                                                                                   edges]

    def key(x: (Point2d | (Point2d, Point2d), bool)):
        s: Point2d | (Point2d, Point2d) = x[0]
        if isinstance(s, Point2d):
            return s.y - 0.00001
        else:
            return max(s[0].y, s[1].y)

    points_and_edges.sort(key=key)
    for x in points_and_edges:
        if x[1]:
            psd.add_edge(*x[0])
        else:
            psd.add_vertex(x[0])
    return psd


def quadratic_tiling(contains: Callable[[Point2d], bool], edge_length: float = 0.07) -> PlanarSubdivision:
    num: int = int(1 / edge_length + 2)
    points: list[Point2d] = []
    edges: list[(Point2d, Point2d)] = []
    p: Point2d
    pn: Point2d
    for i in range(-num, num):
        for j in range(-num, num):
            p = Point2d(i * edge_length, j * edge_length)
            if contains(p):
                points.append(p)
                if contains(pn := Point2d((i + 1) * edge_length, j * edge_length)):
                    edges.append((p, pn))
                if contains(pn := Point2d(i * edge_length, (j + 1) * edge_length)):
                    edges.append((p, pn))
    for i in range(-num, num):
        p = Point2d(i * edge_length, num * edge_length)
        if contains(p):
            points.append(p)
    for j in range(-num, num):
        p = Point2d(num * edge_length, j * edge_length)
        if contains(p):
            points.append(p)
    return point_edge_tiling(points, edges)


def hexagonal_tiling(contains: Callable[[Point2d], bool], edge_length: float = 0.07) -> PlanarSubdivision:
    sqthree: float = math.sqrt(3)
    numx: int = int(4 / (3 * edge_length) + 4)
    numy: int = int(1 / (sqthree * edge_length) + 4)

    def point(i: int, j: int) -> Point2d:
        x: float = 0
        y: float = 0
        if i % 4 == 0 or i % 4 == 1:
            y += sqthree / 2
        x += math.floor(i / 2) * 1.5
        if i % 2 == 1:
            x += 1
        y += j * sqthree
        return Point2d(x * edge_length, y * edge_length)

    points: list[Point2d] = [point(i, j) for i in range(-numx, numx) for j in range(-numy, numy) if
                             contains(point(i, j))]
    edges: list[(Point2d, Point2d)] = []
    for p in points:
        for q in points:
            if p != q and abs(p - q) <= edge_length * 1.00001 and (q, p) not in edges:
                edges.append((p, q))
    return point_edge_tiling(points, edges)


class PenroseHalfRhomb:
    p1: Point2d
    p2: Point2d
    p3: Point2d

    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def inflate(self) -> list[PenroseHalfRhomb]:
        raise NotImplementedError


def golden_midpoint(p1: Point2d, p2: Point2d) -> Point2d:
    phi: float = (5 ** .5 - 1) / 2
    return p1 + phi * (p2 - p1)


class Penrose_B(PenroseHalfRhomb):
    def inflate(self) -> list[PenroseHalfRhomb]:
        mp1: Point2d = golden_midpoint(self.p1, self.p3)
        mp2: Point2d = golden_midpoint(self.p1, self.p2)
        B1: Penrose_B = Penrose_B(mp1, mp2, self.p1)
        B2: Penrose_B = Penrose_B(self.p3, mp1, self.p2)
        A1: Penrose_A = Penrose_A(mp2, mp1, self.p2)
        return [B1, B2, A1]


class Penrose_A(PenroseHalfRhomb):
    def inflate(self) -> list[PenroseHalfRhomb]:
        mp: Point2d = golden_midpoint(self.p2, self.p1)
        A2: Penrose_A = Penrose_A(mp, self.p3, self.p1)
        B3: Penrose_B = Penrose_B(self.p3, mp, self.p2)
        return [A2, B3]


def rotate(p, alph):
    from math import sin, cos
    sa = sin(alph)
    ca = cos(alph)
    return Point2d(ca * p.x - sa * p.y, ca * p.y + sa * p.x)


def penrose_half_rhomb_tiling(p1, p2, n):  # Sieht im Moment noch sehr verzerrt aus.
    from math import pi
    dir = p1 - p2
    p3 = p2 + rotate(dir, -pi / 5)
    unit_B: Penrose_B = Penrose_B(p1, p2, p3)
    half_rhombs = [unit_B]
    for _ in range(n):
        new_half_rhombs = []
        for hr in half_rhombs:
            new_half_rhombs.extend(hr.inflate())
        half_rhombs = new_half_rhombs
    points = set()
    for hr in half_rhombs:
        points.add(hr.p1)
        points.add(hr.p2)
        points.add(hr.p3)
    points = list(points)
    edges = set()
    for hr in half_rhombs:
        if (hr.p1, hr.p2) not in edges and (hr.p2, hr.p1) not in edges:
            edges.add((hr.p1, hr.p2))
        if (hr.p2, hr.p3) not in edges and (hr.p3, hr.p2) not in edges:
            edges.add((hr.p2, hr.p3))
    edges = list(edges)
    return point_edge_tiling(points, edges)
