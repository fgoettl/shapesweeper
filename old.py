from math import sqrt, cos, pi, sin
from rendering import Renderable
from geometry import Point2d


def hexcoords(x, y, k, r=0.1, d=0.02):
    center_dist = r * sqrt(3) + d
    center_x, center_y = ((0.5 * y + x) * center_dist, sqrt(3) / 2 * y * center_dist)
    intern_x, intern_y = (cos(pi / 3 * k + pi / 6), sin(pi / 3 * k + pi / 6))
    return center_x + intern_x * r, center_y + intern_y * r, 0, 1 - (intern_y + 1) / 2, (intern_x + 1) / 2


def hexcenter(x, y, r=0.1, d=0.02):
    center_dist = r * sqrt(3) + d
    return (0.5 * y + x) * center_dist, sqrt(3) / 2 * y * center_dist


def hexindices(n):
    ind = [0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5]
    return [i + 6 * n for i in ind]


class Hextile(Renderable):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y
        self.neighbours = []
        self.bomb = False
        self.adj_bombs = 0
        self.status = 0

    def find_neighbours(self, tiles):
        self.neighbours = []
        for t in tiles:
            if ((t.x == self.x + 1 or t.x == self.x - 1) and t.y == self.y) or (
                    (t.y == self.y - 1 or t.y == self.y + 1) and t.x == self.x) or (
                    t.x == self.x - 1 and t.y == self.y + 1) or (t.x == self.x + 1 and t.y == self.y - 1):
                self.neighbours.append(t)

    def set_bomb(self, b=True):
        self.bomb = b

    def calc_adj_bombs(self):
        self.adj_bombs = sum([t.bomb for t in self.neighbours])

    def get_static_data(self):
        r = 0.05
        d = 0.01
        vertices = []
        for k in range(6):
            vertices.extend(hexcoords(self.x, self.y, k, r, d)[:3])
        coord = []
        for k in range(6):
            coord.extend(hexcoords(self.x, self.y, k, r, d)[3:])
        if self.bomb:
            underlying = [7] * 6
        else:
            underlying = [self.adj_bombs for _ in range(6)]
        return vertices, coord, underlying

    def get_dynamic_data(self):
        status = [self.status for _ in range(6)]
        return status,

    def get_stream_data(self):
        return ()

    def get_element_indices(self):
        return hexindices(0)

    def get_n_vertices(self):
        return 6

    def get_status(self):
        return [self.status for _ in range(6)]

    def is_hit(self, x, y, r=0.05, d=0.01):
        sx, sy = hexcenter(self.x, self.y, r, d)
        dx = abs(x - sx)
        dy = abs(y - sy)
        b1 = (dx < sqrt(3) / 2 * r)
        b2 = (dy < -r / sqrt(3) * dx + r)
        return b1 and b2

    def reveal(self):
        if self.bomb and self.status == 0:
            print("LOST")
            self.set_exploded()
        if self.status == 0:
            self.status = 1
            self.dynamic_data_changed()
            if self.adj_bombs == 0 and not self.bomb:
                for n in self.neighbours:
                    n.reveal()

    def flag(self):
        if self.status == 0:
            self.status = 2
            self.dynamic_data_changed()
        elif self.status == 2:
            self.status = 0
            self.dynamic_data_changed()


def test_sd():
    from tilings import hexagonal_tiling
    from geometry import in_circle, in_rectangle
    def f(x: Point2d) -> bool:
        return in_circle(x, Point2d(0, 0), 0.7)

    def g(x: Point2d) -> bool:
        return in_rectangle(x, -0.2, 0.8, -0.3, 0.6)

    psd = hexagonal_tiling(f, edge_length=0.03)
    return psd.inner_vertex_dual()
