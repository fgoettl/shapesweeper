from __future__ import annotations
from typing import Dict
from geometry import Point2d, left_turn, Polygon


class Vertex:
    coordinates: Point2d | None
    incident_edge: HalfEdge | None
    containing_face: Face | None  # needs to be correct only of the vertex is isolated

    def __init__(self):
        self.coordinates = None
        self.incident_edge = None
        self.containing_face = None

    def incident_edges(self) -> list[HalfEdge]:
        edges: list[HalfEdge] = []
        if self.incident_edge is not None:
            start: HalfEdge = self.incident_edge
            edges.append(start)
            current: HalfEdge = start.next_around_origin
            while current != start:
                edges.append(current)
                current = current.next_around_origin
        return edges

    def __repr__(self) -> str:
        return f"Vertex(coord: {self.coordinates})"


def lex_leq(x1: (int, float), x2: (int, float)) -> bool:
    return x1[0] < x2[0] or (x1[0] == x2[0] and x1[1] <= x2[1])


def calculate_prev_for_insert(new_target: Vertex, edges: list[HalfEdge]) -> HalfEdge | None:
    if not edges:
        return None
    origin: Vertex = edges[0].origin

    def normalized_direction(w: Vertex) -> (int, float):
        direction: Point2d = w.coordinates - origin.coordinates
        i: int = 1 if direction.x >= 0 else -1
        f: float = direction.y * i / abs(direction)
        return i, f

    def counterclockwise(v1: Vertex, v2: Vertex, v3: Vertex):
        d1: (int, float) = normalized_direction(v1)
        d2: (int, float) = normalized_direction(v2)
        d3: (int, float) = normalized_direction(v3)
        c1: bool = lex_leq(d1, d2)
        c2: bool = lex_leq(d2, d3)
        c3: bool = lex_leq(d3, d1)
        return [c1, c2, c3].count(True) == 2

    e: HalfEdge = edges[0]
    v: Vertex = e.target
    for e_ in edges[1:]:
        if counterclockwise(v, e_.target, new_target):
            e = e_
            v = e.target
    return e


class HalfEdge:
    origin: Vertex | None
    twin: HalfEdge | None
    incident_face: Face | None
    next: HalfEdge | None
    prev: HalfEdge | None

    def __init__(self):
        self.origin = None
        self.twin = None
        self.incident_face = None
        self.next = None
        self.prev = None

    @property
    def target(self):
        return self.twin.origin

    @property
    def next_around_origin(self) -> HalfEdge:
        return self.prev.twin

    @property
    def cycle(self) -> list[HalfEdge]:
        c: list[HalfEdge] = [self]
        current_edge: HalfEdge = self.next
        while current_edge != self:
            c.append(current_edge)
            current_edge = current_edge.next
        return c

    def __repr__(self) -> str:
        return f"HalfEdge(origin: {self.origin}, target: {self.target})"  # , face: {self.incident_face})"


class Face:
    ID = 0
    outer_component: HalfEdge | None
    inner_components: list[HalfEdge] | None
    isolated_vertices: set[Vertex] | None

    def __init__(self):
        self.outer_component = None
        self.inner_components = None
        self.isolated_vertices = None
        self.id = Face.ID
        Face.ID += 1

    def contains(self, p: Point2d) -> bool:
        c: bool
        if self.outer_component is None:
            c = True
        else:
            c = (winding_number(self.outer_component, p) == 1)
        if self.inner_components is not None:
            for e in self.inner_components:
                if winding_number(e, p) == 1:
                    c = False
        return c

    def on_outer_cycle(self, e: HalfEdge) -> bool:
        if self.outer_component is None:
            return False
        return e in self.outer_component.cycle

    def on_inner_cycle(self, e: HalfEdge) -> bool:
        for i in self.inner_components:
            if e in i.cycle:
                return True
        return False

    def inner_cycle_edge(self, e: HalfEdge) -> HalfEdge | None:
        for i in self.inner_components:
            if e in i.cycle:
                return i
        return None

    def __repr__(self) -> str:
        return f"Face(ID: {self.id}), outer: {self.outer_component})"

    def outer_polygon(self) -> Polygon | None:
        if self.outer_component is not None:
            start = self.outer_component
            vertices = [(start.origin.coordinates.x, start.origin.coordinates.y)]
            current = start.next
            while current != start:
                vertices.append((current.origin.coordinates.x, current.origin.coordinates.y))
                current = current.next
            return Polygon(vertices)
        else:
            return None


def winding_number(edge: HalfEdge, p: Point2d):
    def left_of_p(q: Point2d) -> bool:
        return q.x < p.x

    def cross_number_above(e: HalfEdge) -> int:
        po: Point2d = e.origin.coordinates
        pt: Point2d = e.target.coordinates
        if left_of_p(po) and not left_of_p(pt) and left_turn(pt, po, p):
            return -1
        elif not left_of_p(po) and left_of_p(pt) and left_turn(po, pt, p):
            return 1
        else:
            return 0

    return sum([cross_number_above(e) for e in edge.cycle])


class PlanarSubdivision:
    vertices: list[Vertex]
    half_edges: list[HalfEdge]
    faces: list[Face]
    location_to_vertex: Dict[Point2d, Vertex]

    def __init__(self, eps=0.00001):
        self.vertices = []
        self.half_edges = []
        f: Face = Face()
        f.isolated_vertices = set()
        f.inner_components = []
        self.faces = [f]
        self.faces[0].inner_components = []
        self.eps = eps
        self.location_to_vertex = {}

    def containing_face(self, p: Point2d) -> Face | None:
        for f in self.faces:
            if f.contains(p):
                return f
        return None

    def add_vertex(self, p: Point2d) -> PlanarSubdivision:
        v: Vertex = Vertex()
        v.coordinates = p
        f = self.containing_face(p)
        v.containing_face = f
        f.isolated_vertices.add(v)
        self.vertices.append(v)
        self.location_to_vertex[p] = v
        return self

    def add_edge(self, p1: Point2d, p2: Point2d) -> PlanarSubdivision:
        # Calculate vertices that correspond to p1 and p2
        v1: Vertex = self.close_vertex(p1)
        v2: Vertex = self.close_vertex(p2)
        if v1 is None or v2 is None:
            return self
        v1.containing_face.isolated_vertices.discard(v1)
        v2.containing_face.isolated_vertices.discard(v2)

        # Calculate edge around v1 (resp. v2) that will follow the new edge counterclockwise around v1 (resp. v2)
        v1_prev: HalfEdge | None = calculate_prev_for_insert(v2, v1.incident_edges())
        v2_prev: HalfEdge | None = calculate_prev_for_insert(v1, v2.incident_edges())

        # The original cycle containing v1 that gets "interrupted" by the new edge
        v1_prev_cycle: list[HalfEdge]
        if v1_prev is not None:
            v1_prev_cycle = v1_prev.cycle
        else:
            v1_prev_cycle = []

        # Create new HalfEdges he1:v1->v2 and he2:v2->v1
        he1: HalfEdge = HalfEdge()
        he2: HalfEdge = HalfEdge()

        # Set Origins and Twins
        he1.origin = v1
        he2.origin = v2
        he1.twin = he2
        he2.twin = he1

        # Insert he1 and he2 properly into the edges around v1 and v2
        if v1_prev is not None:
            v1_next: HalfEdge = v1_prev.next_around_origin

            he1.prev = v1_next.twin
            v1_next.twin.next = he1

            he2.next = v1_prev
            v1_prev.prev = he2
        else:
            he1.prev = he2
            he2.next = he1
        if v2_prev is not None:
            v2_next: HalfEdge = v2_prev.next_around_origin

            he2.prev = v2_next.twin
            v2_next.twin.next = he2

            he1.next = v2_prev
            v2_prev.prev = he1
        else:
            he2.prev = he1
            he1.next = he2

        # Set correct incident faces
        if v1_prev is None:
            he1.incident_face = v1.containing_face
            he2.incident_face = v1.containing_face
        elif v2_prev is None:
            he1.incident_face = v2.containing_face
            he2.incident_face = v2.containing_face
        else:  # TODO!!! We don't handle the case where v1 or v2 is contained in containing_face/old_face
            if v2_prev in v1_prev_cycle:
                old_face: Face = v1_prev.incident_face
                # The new edge divides old_face into two
                inner_components: list[HalfEdge] = old_face.inner_components
                isolated_vertices: set[Vertex] = old_face.isolated_vertices
                new_face: Face = Face()

                if not (old_face.on_outer_cycle(he1) or old_face.on_outer_cycle(he2)):
                    # The new edge is not incident to the outer_component of old_face
                    # Remove the old inner component from the old_face
                    old_face.inner_components = [e for e in old_face.inner_components if e not in v1_prev_cycle]

                    mid_point: Point2d = 0.5 * (he1.origin.coordinates + he1.target.coordinates)
                    direction: Point2d = he1.target.coordinates - he1.origin.coordinates
                    direction = (1 / abs(direction)) * direction
                    test_point: Point2d = mid_point + 0.000001 * Point2d(-direction.y, direction.x)

                    inner_he: HalfEdge
                    outer_he: HalfEdge
                    if winding_number(he1, test_point) != 0:
                        outer_he = he2
                        inner_he = he1
                    else:
                        outer_he = he1
                        inner_he = he2
                    new_face.outer_component = inner_he
                    old_face.inner_components.append(outer_he)
                    for e in outer_he.cycle:
                        e.incident_face = old_face
                    for e in inner_he.cycle:
                        e.incident_face = new_face
                else:
                    old_face.outer_component = he1
                    new_face.outer_component = he2
                    for e in he1.cycle:
                        e.incident_face = old_face
                    for e in he2.cycle:
                        e.incident_face = new_face
                old_face.inner_components = []
                new_face.inner_components = []
                for e in inner_components:
                    if old_face.contains(e.origin.coordinates):
                        old_face.inner_components.append(e)
                    else:
                        new_face.inner_components.append(e)
                old_face.isolated_vertices = set()
                new_face.isolated_vertices = set()
                for v in isolated_vertices:
                    if old_face.contains(v.coordinates):
                        old_face.isolated_vertices.add(v)
                        v.containing_face = old_face
                    else:
                        new_face.isolated_vertices.add(v)
                        v.containing_face = new_face
                self.faces.append(new_face)
            else:
                # The new edge connects two inner components of containing_face
                containing_face: Face = v1_prev.incident_face
                he1.incident_face = containing_face
                he2.incident_face = containing_face
                containing_face.inner_components = [e for e in containing_face.inner_components if
                                                    e not in v1_prev_cycle]

        # Set correct incident edges
        v1.incident_edge = he1
        v2.incident_edge = he2
        self.half_edges.append(he1)
        self.half_edges.append(he2)
        return self

    def remove_vertex(self, p: Point2d) -> PlanarSubdivision:
        return self

    def remove_edge(self, p1: Point2d, p2: Point2d) -> PlanarSubdivision:
        return self

    def is_vertex(self, p: Point2d):
        for v in self.vertices:
            if abs(v.coordinates - p) < self.eps:
                return True
        return False

    def close_vertex(self, p: Point2d) -> Vertex | None:
        if p in self.location_to_vertex:
            return self.location_to_vertex[p]
        for v in self.vertices:
            if abs(v.coordinates - p) < self.eps:
                return v
        return None

    def outer_face(self) -> Face | None:
        for f in self.faces:
            if f.outer_component is None:
                return f
        return None

    def inner_dual(self) -> (list[Face], list[(Face, Face)]):
        outer_f: Face = self.outer_face()
        vs, es = self.dual_graph()
        return [v for v in vs if v != outer_f], [e for e in es if e[0] != outer_f and e[1] != outer_f]

    def inner_vertex_dual(self) -> (list[Face], list[(Face, Face)]):
        outer_f: Face = self.outer_face()
        vs, es = self.vertex_dual()
        return [v for v in vs if v != outer_f], [e for e in es if e[0] != outer_f and e[1] != outer_f]

    def dual_graph(self) -> (list[Face], list[(Face, Face)]):
        vertices: list[Face] = [f for f in self.faces]
        edges: list[(Face, Face)] = []
        face1: Face
        face2: Face
        for he in self.half_edges:
            face1 = he.incident_face
            face2 = he.twin.incident_face
            if face1 != face2 and (face1, face2) not in edges:
                edges.append((face1, face2))
        return vertices, edges

    def vertex_dual(self) -> (list[Face], list[(Face, Face)]):
        vertices: list[Face] = [f for f in self.faces]
        edges: list[(Face, Face)] = []
        halfedges: list[HalfEdge]
        face1: Face
        face2: Face
        faces: list[Face]
        for vertex in self.vertices:
            if vertex.incident_edge is None:
                continue
            halfedges = vertex.incident_edges()
            faces = [he.incident_face for he in halfedges]
            for face1 in faces:
                for face2 in faces:
                    if face1 != face2:
                        edges.append((face1, face2))
        return vertices, list(set(edges))
