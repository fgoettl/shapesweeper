#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in int underlying;
layout (location = 3) in int status;

out vec2 coords;
flat out int stat;
flat out int underl;

void main()
{
    coords = texcoord;
    stat = status;
    underl = underlying;
    gl_Position = vec4(position, 1.0f);
}