#version 330 core

in vec2 coords;
flat in int stat;
flat in int underl;

uniform bool exploded;

out vec4 outColor;

//uniform sampler2D ourTexture;
vec4 dot(vec2 coords, vec2 center, float radius, vec4 col){
    float x = coords.x - center.x;
    float y = coords.y - center.y;
    if(x*x+y*y <= radius*radius){
        return col;
    }
    else{
        return vec4(1.0, 1.0, 1.0, 0.0);
    }
}

vec4 overlay(vec4 under, vec4 over){
    return under*(1-over.w)+over*over.w;
}

vec4 number(vec2 coords, int n){
    float r = 0.05;
    float spacing = 0.15;
    vec4 dotcol = vec4(0.2,0.2,0.2,1.0);
    vec4 col = vec4(1.0, 1.0, 1.0, 0.0);
    if(n == 1 || n==3 || n==5){
    col = overlay(col, dot(coords, vec2(0.5, 0.5), r, dotcol));
    }
    if(n == 2 || n == 3 || n == 4 || n == 6 || n == 5){
    col = overlay(col, dot(coords, vec2(0.5-spacing, 0.5-spacing), r, dotcol));
    }
    if(n == 6){
    col = overlay(col, dot(coords, vec2(0.5, 0.5-spacing), r, dotcol));
    }
    if(n == 4 || n == 6 || n == 5){
    col = overlay(col, dot(coords, vec2(0.5-spacing, 0.5+spacing), r, dotcol));
    }
    if(n == 4 || n == 6 || n == 5){
    col = overlay(col, dot(coords, vec2(0.5+spacing, 0.5-spacing), r, dotcol));
    }
    if(n == 6){
    col = overlay(col, dot(coords, vec2(0.5, 0.5+spacing), r, dotcol));
    }
    if(n == 2 || n == 3 || n == 4 || n == 6 || n == 5){
    col = overlay(col, dot(coords, vec2(0.5+spacing, 0.5+spacing), r, dotcol));
    }
    return col;
}

vec4 flag(vec2 coords){
    float x = coords.x-0.5;
    float y = coords.y-0.5;
    if(y<=0.2 && x <= 0.5*y+0.15 && x >= -0.5*y -0.15){
    return vec4(1.0, 0.0, 0.0, 1.0);
    }
    else return vec4(1.0, 1.0, 1.0, 0.0);
}

vec4 bomb(vec2 coords){
    return dot(coords, vec2(0.5,0.5), 0.3, vec4(0.0, 0.0, 0.0, 1.0));
}



void main()
{
    vec4 c_col = vec4(0.8, 0.8, 0.8, 1.0);
    vec4 uc_col = vec4(0.4, 0.4, 0.4, 1.0);
    vec4 e_col = vec4(1.0,0.2,0.2,1.0);
    outColor = vec4(1.0, 1.0, 1.0, 1.0);
    if(stat == 0){ //covered tile
        if(exploded && underl == 7){
            outColor = overlay(uc_col, bomb(coords));
        }
        else{
            outColor = c_col;
        }
    }
    else if(stat == 2){ //flagged tile
        if(exploded){
            outColor = uc_col;
            if(underl == 7){
                outColor = overlay(outColor, bomb(coords));
            }
        }
        else{
            outColor = c_col;
        }
        outColor = overlay(outColor, flag(coords));
    }
    else if(stat == 3){ //pressed tile
        outColor = uc_col;
    }
    else if(stat == 1){ //uncovered tile
        if(underl == 7){
            outColor = overlay(e_col, bomb(coords));
        }
        else{
            outColor = overlay(uc_col, number(coords, underl));
        }
    }
}