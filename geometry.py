from __future__ import annotations
from typing import NamedTuple
import math


class Point2d(NamedTuple):
    x: float
    y: float

    def __add__(self, other: Point2d) -> Point2d:
        return Point2d(self.x + other.x, self.y + other.y)

    def __sub__(self, other: Point2d) -> Point2d:
        return Point2d(self.x - other.x, self.y - other.y)

    def __or__(self, other: Point2d) -> float:
        return self.x * other.x + self.y * other.y

    def __rmul__(self, other: float) -> Point2d:
        return Point2d(self.x * other, self.y * other)

    def __abs__(self) -> float:
        return math.sqrt(self | self)

    def __repr__(self) -> str:
        return f"Point2d(x: {self.x}, y: {self.y})"


def left_turn(p1: Point2d, p2: Point2d, p3: Point2d) -> bool:
    return Point2d(p1.y - p2.y, p2.x - p1.x) | (p3 - p2) > 0


def in_circle(p: Point2d, center: Point2d, radius: float) -> bool:
    return abs(p - center) <= radius


def in_rectangle(p: Point2d, xmin: float, xmax: float, ymin: float, ymax: float) -> bool:
    return xmin <= p.x <= xmax and ymin <= p.y <= ymax



class Polygon:
    def __init__(self, points):
        """
        :param points: List of Points of the polygon in counterclockwise order. Each Point is a pair (x,y).
        """
        self.points = points
        self.xmin = min(x for x, y in points)
        self.xmax = max(x for x, y in points)
        self.ymin = min(y for x, y in points)
        self.ymax = max(y for x, y in points)  #
        size = max(self.xmax - self.xmin, self.ymax - self.ymin)
        self.relative_points = [
            (0.5 + (p[0] - (self.xmax + self.xmin) / 2) / size, 0.5 + (p[1] - (self.ymax + self.ymin) / 2) / size) for p
            in self.points]

    def is_inside(self, point):  # works only for convex polygons
        px, py = point
        if px > self.xmax or px < self.xmin or py > self.ymax or py < self.ymin:
            return False
        p = self.points[-1]
        for q in self.points:
            if (p[1] - q[1]) * (px - q[0]) + (q[0] - p[0]) * (py - q[1]) < 0:
                return False
            p = q
        return True


def line_through(p1, p2):
    r"""
    :param p1: 2d-point (x,y)
    :param p2: 2d-point (x,y)
    :return: line through p1 and p2 in the direction from p1 to p2 in form (point, direction)
    """
    return p1, (p2[0] - p1[0], p2[1] - p1[1])


def line_intersection(l1, l2):
    r"""
    :param l1: line in form (point, direction)
    :param l2: line in form (point, direction)
    :return: intersection of l1 with l2 as 2d-point (x,y) if l1 and l2 are not parallel; None otherwise
    """
    EPS = 0.00001
    l1p, l1d = l1
    l2p, l2d = l2
    a1, b1 = l1p
    a2, b2 = l2p
    r1, s1 = l1d
    r2, s2 = l2d
    D = s1 * r2 - s2 * r1
    if abs(D) < EPS:
        return None
    lam = (s2 * (a1 - a2) + r2 * (b2 - b1)) / D
    return a1 + lam * r1, b1 + lam * s1


def offset_line(l, d):
    r"""
    :param l: line in form (point, direction)
    :param d: real
    :return: line in form (point, direction) that is parallel to l and moved d to the left (with respect to the direction of l)
    """
    p, r = l
    rabs = math.sqrt(r[0] * r[0] + r[1] * r[1])
    rtx, rty = -r[1] * d / rabs, r[0] * d / rabs
    return (p[0] + rtx, p[1] + rty), r


def offset_poly(poly, d=0.01):  # works only for convex polygons with suitably long sides
    r"""
    :param poly: Polygon, has to be convex and have suitably long edges
    :param d: real
    :return: Polygon that is obtained from poly by offsetting each edge by d to the inside
    """
    new_points = []
    n = len(poly.points)
    for j in range(n):
        i = (j - 1) % n
        k = (j + 1) % n
        l1 = line_through(poly.points[i], poly.points[j])
        l2 = line_through(poly.points[j], poly.points[k])
        new_points.append(line_intersection(offset_line(l1, d), offset_line(l2, d)))
    return Polygon(new_points)


class Triangulation:
    def __init__(self, triangles):
        """
        :param triangles: List of triangles, each given as triple of the indices of its points.
        """
        self.triangles = triangles


def naive_triangulation(poly):  # works only for polygons which are star shaped with respect to poly.points[0]
    r"""
    :param poly: Polygon, has to be star shaped with respect to poly.points[0]
    :return: Triangulation of poly
    """
    n = len(poly.points)
    triangles = []
    if len(poly.points) > 2:
        a = 0
        b = 1
        for c in range(2, n):
            triangles.append((a, b, c))
            b = c
    else:
        print("ERROR POLY POINTS < 3")
    return Triangulation(triangles)
