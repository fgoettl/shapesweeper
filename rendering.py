import pyglet.gl as GL
import ctypes as C
from shaderloader import ShaderProgram
from minimal_utility import gen_vao, gen_vbo, gen_ebo


class RenderDataBroker:
    def __init__(self, static_data_format, dynamic_data_format, stream_data_format):
        self.static_data_format = static_data_format
        self.dynamic_data_format = dynamic_data_format
        self.stream_data_format = stream_data_format
        self.renderables = []
        self.n_renderables = 0
        self.renderables_changed = False
        self.dynamic_data_changed = True

    def get_data_format(self):
        return self.static_data_format, self.dynamic_data_format, self.stream_data_format

    def register_renderable(self, renderable):
        if renderable._rdb is not None:
            print("ERROR: RENDERABLE ALREADY REGISTERED AT SOME RDB")
            return
        self.renderables.append(renderable)
        renderable.set_RenderDataBroker(self, self.n_renderables)
        self.n_renderables += 1
        self.renderables_changed = True

    def unregister_renderable(self, renderable):
        if renderable._rdb != self:
            print("ERROR: RENDERABLE NOT REGISTERED AT THIS RDB")
            return
        rid = renderable.get_Renderable_ID()
        x = self.renderables.pop()
        if rid != self.n_renderables - 1:
            self.renderables[rid] = x
            x.set_RenderDataBroker(self, rid)
        renderable.set_RenderDataBroker(None, 0)
        self.n_renderables -= 1
        self.renderables_changed = True

    def notify_dynamic_data_changed(self):
        self.dynamic_data_changed = True

    def get_static_data(self):
        data = []
        for _ in range(len(self.static_data_format)):
            data.append([])
        for r in self.renderables:
            d = r.get_static_data()
            for x, y in zip(d, data):
                y.extend(x)
        return data

    def get_dynamic_data(self):
        data = []
        for _ in range(len(self.dynamic_data_format)):
            data.append([])
        for r in self.renderables:
            d = r.get_dynamic_data()
            for x, y in zip(d, data):
                y.extend(x)
        return data

    def get_stream_data(self):
        data = []
        for _ in range(len(self.stream_data_format)):
            data.append([])
        for r in self.renderables:
            d = r.get_stream_data()
            for x, y in zip(d, data):
                y.extend(x)
        return data

    def get_element_indices(self):
        indices = []
        l = 0
        for r in self.renderables:
            ind = r.get_element_indices()
            indices.extend([i + l for i in ind])
            l += r.get_n_vertices()
        return indices


class Renderer:
    def __init__(self, rdb, vertex_shader_path, fragment_shader_path):
        self.rdb = rdb
        self.shaderProgram = ShaderProgram(vertex_shader_path, fragment_shader_path)
        self.vao = gen_vao()
        GL.glBindVertexArray(self.vao)
        self.static_data_format, self.dynamic_data_format, self.stream_data_format = rdb.get_data_format()
        self.static_vbos = []
        self.dynamic_vbos = []
        self.stream_vbos = []
        self.initialize_vbos()
        self.initialize_ebo()
        self.element_num = 0

    def initialize_ebo(self):
        self.ebo = gen_ebo()

    def initialize_vbos(self):
        location = 0
        for f in self.static_data_format:
            vbo = gen_vbo()
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo)
            if f[1] == GL.GL_FLOAT:
                GL.glVertexAttribPointer(location, f[2], GL.GL_FLOAT, GL.GL_FALSE, f[2] * C.sizeof(C.c_float()),
                                         C.c_void_p(0))
            elif f[1] == GL.GL_INT:
                GL.glVertexAttribIPointer(location, f[2], GL.GL_INT, f[2] * C.sizeof(C.c_int()), C.c_void_p(0))
            else:
                print("ERROR:FORMAT:ILLEGAL TYPE")
            GL.glEnableVertexAttribArray(location)
            self.static_vbos.append(vbo)
            location += 1
        for f in self.dynamic_data_format:
            vbo = gen_vbo()
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo)
            if f[1] == GL.GL_FLOAT:
                GL.glVertexAttribPointer(location, f[2], GL.GL_FLOAT, GL.GL_FALSE, f[2] * C.sizeof(C.c_float()),
                                         C.c_void_p(0))
            elif f[1] == GL.GL_INT:
                GL.glVertexAttribIPointer(location, f[2], GL.GL_INT, f[2] * C.sizeof(C.c_int()), C.c_void_p(0))
            else:
                print("ERROR:FORMAT:ILLEGAL TYPE")
            GL.glEnableVertexAttribArray(location)
            self.dynamic_vbos.append(vbo)
            location += 1
        for f in self.stream_data_format:
            vbo = gen_vbo()
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo)
            if f[1] == GL.GL_FLOAT:
                GL.glVertexAttribPointer(location, f[2], GL.GL_FLOAT, GL.GL_FALSE, f[2] * C.sizeof(C.c_float()),
                                         C.c_void_p(0))
            elif f[1] == GL.GL_INT:
                GL.glVertexAttribIPointer(location, f[2], GL.GL_INT, f[2] * C.sizeof(C.c_int()), C.c_void_p(0))
            else:
                print("ERROR:FORMAT:ILLEGAL TYPE")
            GL.glEnableVertexAttribArray(location)
            self.stream_vbos.append(vbo)
            location += 1

    def pull_static_data(self):
        data = self.rdb.get_static_data()
        for d, f, v in zip(data, self.static_data_format, self.static_vbos):
            if f[1] == GL.GL_FLOAT:
                type = C.c_float
            else:
                type = C.c_int
            c_vertex_data = (type * len(d))(*d)
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, v)
            GL.glBufferData(GL.GL_ARRAY_BUFFER, len(d) * C.sizeof(type()), C.byref(c_vertex_data),
                            GL.GL_STATIC_DRAW)

    def pull_dynamic_data(self):
        data = self.rdb.get_dynamic_data()
        for d, f, v in zip(data, self.dynamic_data_format, self.dynamic_vbos):
            if f[1] == GL.GL_FLOAT:
                type = C.c_float
            else:
                type = C.c_int
            c_vertex_data = (type * len(d))(*d)
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, v)
            GL.glBufferData(GL.GL_ARRAY_BUFFER, len(d) * C.sizeof(type()), C.byref(c_vertex_data),
                            GL.GL_DYNAMIC_DRAW)

    def pull_stream_data(self):
        data = self.rdb.get_stream_data()
        for d, f, v in zip(data, self.stream_data_format, self.stream_vbos):
            if f[1] == GL.GL_FLOAT:
                type = C.c_float
            else:
                type = C.c_int
            c_vertex_data = (type * len(d))(*d)
            GL.glBindBuffer(GL.GL_ARRAY_BUFFER, v)
            GL.glBufferData(GL.GL_ARRAY_BUFFER, len(d) * C.sizeof(type()), C.byref(c_vertex_data),
                            GL.GL_STREAM_DRAW)

    def pull_element_indices(self):
        xindices = self.rdb.get_element_indices()
        indices = (C.c_uint * len(xindices))(*xindices)
        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, self.ebo)
        GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, C.sizeof(indices), C.byref(indices),
                        GL.GL_STATIC_DRAW)
        self.element_num = len(xindices)

    def pull_changes(self):
        if self.rdb.renderables_changed:
            self.pull_static_data()
            self.pull_dynamic_data()
            self.pull_element_indices()
        elif self.rdb.dynamic_data_changed:
            self.pull_dynamic_data()
        if self.stream_data_format:
            self.pull_stream_data()
        self.rdb.renderables_changed = False
        self.rdb.dynamic_data_changed = False

    def set_uniform_bool(self, name, value):
        self.shaderProgram.use()
        self.shaderProgram.setBool(name, value)

    def set_uniform_int(self, name, value):
        self.shaderProgram.use()
        self.shaderProgram.setInt(name, value)

    def set_uniform_float(self, name, value):
        self.shaderProgram.use()
        self.shaderProgram.setFloat(name, value)

    def set_uniform_float4(self, name, *values):
        self.shaderProgram.use()
        self.shaderProgram.setFloat4(name, *values)

    def set_uniform_mat4(self, name, matrix):
        self.shaderProgram.use()
        self.shaderProgram.setMat4(name, matrix)

    def render(self):
        self.pull_changes()
        self.shaderProgram.use()
        GL.glBindVertexArray(self.vao)
        GL.glDrawElements(GL.GL_TRIANGLES, self.element_num, GL.GL_UNSIGNED_INT, C.c_void_p(0))


class Renderable:

    def __init__(self):
        self._rdb = None
        self._rid = 0

    def set_RenderDataBroker(self, rdb, rid):
        self._rdb = rdb
        self._rid = rid

    def get_Renderable_ID(self):
        return self._rid

    def dynamic_data_changed(self):
        if self._rdb is not None:
            self._rdb.notify_dynamic_data_changed()

    def get_n_vertices(self):
        raise NotImplementedError

    def get_static_data(self):
        raise NotImplementedError

    def get_dynamic_data(self):
        raise NotImplementedError

    def get_stream_data(self):
        raise NotImplementedError

    def get_element_indices(self):
        raise NotImplementedError
