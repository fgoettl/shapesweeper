from random import randint


def shuffle(L):
    for i in range(len(L)):
        j = randint(i, len(L) - 1)
        t = L[i]
        L[i] = L[j]
        L[j] = t


def flatten(L):
    r = []
    for l in L:
        r.extend(l)
    return r
