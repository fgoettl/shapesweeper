import pyglet.gl as GL
import ctypes as C


def str_to_C(string):
    return C.cast(C.c_char_p(bytes(string, 'utf-8')), C.POINTER(C.c_char))


class ShaderProgram:

    def __init__(self, vertexPath, fragmentPath):
        # Read Files
        v_source = ""
        f_source = ""
        try:
            vf = open(vertexPath, "r")
            ff = open(fragmentPath, "r")
            v_source = vf.read()
            f_source = ff.read()
            vf.close()
            ff.close()
        except Exception as e:
            print("ERROR::SHADER::LOADING_FILE_FAILED\n", e)

        # Compile VertexShader
        vertexShader = GL.glCreateShader(GL.GL_VERTEX_SHADER)
        vertexSource = str_to_C(v_source)
        GL.glShaderSource(vertexShader, 1, vertexSource, None)
        GL.glCompileShader(vertexShader)
        # Check if VertexShader Compilation was successfull
        status = C.c_int()
        GL.glGetShaderiv(vertexShader, GL.GL_COMPILE_STATUS, C.byref(status))
        if status.value != 1:
            infoLog = C.create_string_buffer(512)
            GL.glGetShaderInfoLog(vertexShader, 512, None, infoLog)
            print("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n", infoLog.value)
        # Compile FragmentShader
        fragmentShader = GL.glCreateShader(GL.GL_FRAGMENT_SHADER)
        fragmentSource = str_to_C(f_source)
        GL.glShaderSource(fragmentShader, 1, fragmentSource, None)
        GL.glCompileShader(fragmentShader)
        # Check if FragmentShader Compilation was successfull
        status = C.c_int()
        GL.glGetShaderiv(fragmentShader, GL.GL_COMPILE_STATUS, C.byref(status))
        if status.value != 1:
            infoLog = C.create_string_buffer(512)
            GL.glGetShaderInfoLog(fragmentShader, 512, None, infoLog)
            print("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n", infoLog.value)
        # Link Shaders into ShaderProgram
        shaderProgram = GL.glCreateProgram()
        GL.glAttachShader(shaderProgram, vertexShader)
        GL.glAttachShader(shaderProgram, fragmentShader)
        GL.glLinkProgram(shaderProgram)
        # Check if Linking was successfull
        status = C.c_int()
        GL.glGetProgramiv(shaderProgram, GL.GL_LINK_STATUS, C.byref(status))
        if status.value != 1:
            infoLog = C.create_string_buffer(512)
            GL.glGetProgramInfoLog(shaderProgram, 512, None, infoLog)
            print("ERROR::SHADERPROGRAM::LINKING_FAILED\n", infoLog.value)
        GL.glDeleteShader(vertexShader)
        GL.glDeleteShader(fragmentShader)
        self.ID = shaderProgram

    def use(self):
        GL.glUseProgram(self.ID)

    def setBool(self, name, value):
        GL.glUniform1i(GL.glGetUniformLocation(self.ID, str_to_C(name)), C.c_int(value))

    def setInt(self, name, value):
        GL.glUniform1i(GL.glGetUniformLocation(self.ID, str_to_C(name)), C.c_int(value))

    def setFloat(self, name, value):
        GL.glUniform1f(GL.glGetUniformLocation(self.ID, str_to_C(name)), C.c_float(value))

    def setFloat4(self, name, value1, value2, value3, value4):
        GL.glUniform4f(GL.glGetUniformLocation(self.ID, str_to_C(name)), C.c_float(value1), C.c_float(value2),
                       C.c_float(value3), C.c_float(value4))

    def setMat4(self, name, matrix):
        GL.glUniformMatrix4fv(GL.glGetUniformLocation(self.ID, str_to_C(name)), 1, GL.GL_TRUE,
                              (C.c_float * len(matrix))(*matrix))
