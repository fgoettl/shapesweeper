import pyglet
import pyglet.gl as GL

from rendering import Renderer, RenderDataBroker, Renderable
from utility import flatten
from geometry import offset_poly, Point2d, naive_triangulation, in_circle, in_rectangle
from utility import shuffle
from tilings import hexagonal_tiling, quadratic_tiling, penrose_half_rhomb_tiling


class PolygonalTile(Renderable):
    def __init__(self, poly, neighbours=None):
        super().__init__()
        if neighbours is None:
            neighbours = []
        self.poly = poly
        self.triangulation = naive_triangulation(poly)

        self.neighbours = neighbours
        self.bomb = False
        self.adj_bombs = 0
        self.calc_adj_bombs()
        self.status = 0

    def set_neighbours(self, neighbours):
        self.neighbours = neighbours
        self.calc_adj_bombs()

    def set_bomb(self, b=True):
        self.bomb = b

    def calc_adj_bombs(self):
        self.adj_bombs = sum([t.bomb for t in self.neighbours])

    def get_static_data(self):
        vertices = flatten([(x, y, 0) for x, y in self.poly.points])
        coord = flatten([(x, y) for x, y in self.poly.relative_points])
        if self.bomb:
            underlying = [7] * len(self.poly.points)
        else:
            underlying = [self.adj_bombs for _ in range(len(self.poly.points))]
        return vertices, coord, underlying

    def get_dynamic_data(self):
        status = [self.status for _ in range(len(self.poly.points))]
        return status,

    def get_stream_data(self):
        return ()

    def get_element_indices(self):
        return flatten(self.triangulation.triangles)

    def get_n_vertices(self):
        return len(self.poly.points)

    def get_status(self):
        return [self.status for _ in range(len(self.poly.points))]

    def is_hit(self, x, y):
        return self.poly.is_inside((x, y))

    def reveal(self):
        if self.bomb and self.status == 0:
            print("LOST")
            self.set_exploded()
        if self.status == 0:
            self.status = 1
            self.dynamic_data_changed()
            if self.adj_bombs == 0 and not self.bomb:
                for n in self.neighbours:
                    n.reveal()

    def flag(self):
        if self.status == 0:
            self.status = 2
            self.dynamic_data_changed()
        elif self.status == 2:
            self.status = 0
            self.dynamic_data_changed()


class OwnWindow(pyglet.window.Window):
    def __init__(self, **kwargs):
        pyglet.window.Window.__init__(self, **kwargs)

        # def f(x) -> bool:
        #    return in_rectangle(x,-0.7,0.4,-1,0.6)#in_circle(x, Point2d(0, 0), 0.7)
        def f(p: Point2d) -> bool:
            return (-0.9 <= p.x <= 0.9 and -0.3 <= p.y <= 0.3) or (-0.9 <= p.y <= 0.9 and -0.3 <= p.x <= 0.3)

        V, E = hexagonal_tiling(f, edge_length=0.06).inner_vertex_dual()

        hextiles = [(x, PolygonalTile(offset_poly(x.outer_polygon(), 0.005))) for x in V]
        bomb_num = 60
        shuffle(hextiles)
        for i, t in zip(range(bomb_num), hextiles):
            t[1].set_bomb(True)
        for x, t in hextiles:  # besser machen
            _neighb = [n[0] for n in E if n[1] == x]  # Sortieren oder Adj.-Liste
            neighb = [a[1] for a in hextiles if a[0] in _neighb]
            t.set_neighbours(neighb)
        self.RDB = RenderDataBroker(
            [("position", GL.GL_FLOAT, 3), ("texcoord", GL.GL_FLOAT, 2), ("underlying", GL.GL_INT, 1)],
            [("status", GL.GL_INT, 1)],
            [])
        hextiles = [x[1] for x in hextiles]
        for ht in hextiles:
            self.RDB.register_renderable(ht)
        self.R = Renderer(self.RDB, "shaders/tile.vs", "shaders/tile.fs")

        def se():
            self.R.set_uniform_bool("exploded", True)

        self.R.set_uniform_bool("exploded", False)
        for he in hextiles:
            he.set_exploded = se
        self.hextiles = hextiles

        GL.glClearColor(0.0, 0.0, 0.0, 1)

    def on_draw(self):
        GL.glClear(GL.GL_COLOR_BUFFER_BIT)
        self.R.render()

    def on_mouse_press(self, x, y, button, modifiers):
        if button == pyglet.window.mouse.LEFT:
            nx = (x - 450) / 450
            ny = (y - 450) / 450
            for t in self.hextiles:
                if t.is_hit(nx, ny):
                    t.reveal()
                    break
        elif button == pyglet.window.mouse.RIGHT:
            nx = (x - 450) / 450
            ny = (y - 450) / 450
            for t in self.hextiles:
                if t.is_hit(nx, ny):
                    t.flag()
                    break


def main():
    profile = False
    import cProfile

    if profile:
        pr = cProfile.Profile()
        pr.enable()
    config = GL.Config(major_version=3, double_buffer=True, depth_size=24)
    OwnWindow(width=900, height=900, config=config, caption="Hexsweeper!")
    if profile:
        pr.disable()
        pr.print_stats(sort="calls")
    pyglet.app.run()


if __name__ == '__main__':
    main()
